package com.hexagone.product.controllers;

import com.hexagone.product.entities.Product;
import com.hexagone.product.repository.ProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.persistence.EntityNotFoundException;
import java.util.Optional;


@RestController
public class ProductController {

    /**
     * Permet d'utiliser les fonctions de l'interface ProductRepository
     */
    @Autowired
    private ProductRepository productRepository;

    /**
     * Permet d'afficher les produits
     * @return produits
     */
    @GetMapping("/products")
    public Iterable<Product> getProducts(){
        Iterable<Product> products = productRepository.findAll();
        return products;
    }

    /**
     * Permet de créer un produit
     * @param product L'entité product
     * @return
     */
    @PostMapping("/product")
    public Product postProduct(
            @RequestBody
            Product product
    ){
        product = productRepository.save(product);
        return product;
    }

    /**
     * permet d'afficher un produit par rapport à son id
     * @param id ID du produit
     * @return product
     */
    @GetMapping(path = "/product/{id}")
    public Optional<Product> getProduct(@PathVariable("id") Long id){
        Optional<Product> product = productRepository.findById(id);
        return product;
   }

    /**
     * Permet de mettre à jour un produit en fonction de son id
     * @param id ID du produit
     * @param product L'entité product
     * @return command
     */
   @PutMapping("/update/{id}")
   public Product putProduct(
           @PathVariable("id")
           Long id,
           @RequestBody
           Product product
   ){
        Optional<Product> ProductOpt = productRepository.findById(id);
        return ProductOpt.map(currentProduct -> {
            product.setId(currentProduct.getId());
            return productRepository.save(product);
        }).orElseThrow(EntityNotFoundException::new);
   }

    /**
     * Permet de supprimer un produit en fonction de son id
     * @param id ID du produit
     * @return null
     */
   @DeleteMapping("/product/delete/{id}")
   public String deleteProduct(
           @PathVariable
           Long id
   ){
        productRepository.deleteById(id);
        return null;
   }
}
