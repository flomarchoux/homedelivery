package com.hexagone.frontend.interfaces;

import com.hexagone.frontend.entities.Product;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

@FeignClient(value = "produit")
public interface ProductsFeignClient {

    String AUTH_TOKEN = "Authorization";

    /**
     * Afficher tout les produits
     * @return products
     */
    @GetMapping("/products")
    Iterable<Product> products(@RequestHeader(AUTH_TOKEN) String token);

    /**
     * Afficher un produit en particulier à l'aide de son ID
     * @param id est l'ID du produit
     * @return productId
     */
    @GetMapping("/product/{id}")
    Optional<Product> productId(@RequestHeader(AUTH_TOKEN) String token, @PathVariable("id") Long id);

    /**
     * Supprimer un produit à l'aide de son ID
     * @param id est l'ID du produit
     * @return deleteProduct
     */
    @DeleteMapping("/product/delete/{id}")
    Product deleteProduct(@RequestHeader(AUTH_TOKEN) String token, @PathVariable("id") Long id);

    /**
     * Mise à jour d'un produit à l'aide de son ID
     * @param id est l'ID du produit
     * @param product est le produit
     * @return putProduct
     */
    @PutMapping("/update/{id}")
    Product putProduct(@RequestHeader(AUTH_TOKEN) String token, @PathVariable("id") Long id, @RequestBody Product product);

    /**
     * Ajouter un nouveau produit
     * @param product est le produit
     * @return product
     */
    @PostMapping("/product")
    Product product(@RequestHeader(AUTH_TOKEN) String token, @RequestBody Product product);

}