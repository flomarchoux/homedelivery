package com.hexagone.frontend.interfaces;

import com.hexagone.frontend.entities.Command;
import com.hexagone.frontend.entities.CommandLine;
import com.hexagone.frontend.entities.Product;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

@FeignClient(value = "command")
public interface CommandsFeignClient {

    String AUTH_TOKEN = "Authorization";

    /**
     * Afficher toute les lignes de commande du panier
     * @return products
     */
    @GetMapping("/commandlines")
    Iterable<CommandLine> commandlines(@RequestHeader(AUTH_TOKEN) String token);

    @GetMapping(path = "/commandline/{id}")
    Optional<CommandLine> commandLineId(@RequestHeader(AUTH_TOKEN) String token, @PathVariable("id") Long id);

    @DeleteMapping("/commandline/delete/{id}")
    Product deleteCommandLine(@RequestHeader(AUTH_TOKEN) String token, @PathVariable("id") Long id);

    @PostMapping("/commandline")
    CommandLine commandLine(@RequestHeader(AUTH_TOKEN) String token, @RequestBody CommandLine commandLine);

    @PutMapping("/commandLine/update/{id}")
    CommandLine putCommandLine(@RequestHeader(AUTH_TOKEN) String token, @PathVariable("id") Long id, @RequestBody CommandLine commandLine);

    @GetMapping("/commands")
    Iterable<Command> commands(@RequestHeader(AUTH_TOKEN) String token);

    @GetMapping("/command/{id}")
    Optional<Command> commandId(@RequestHeader(AUTH_TOKEN) String token, @PathVariable("id") Long id);

    @DeleteMapping("/command/delete/{id}")
    Command deleteCommand(@RequestHeader(AUTH_TOKEN) String token, Long id);

    @PutMapping("/command/update/{id}")
    Command putCommand(@RequestHeader(AUTH_TOKEN) String token, @PathVariable("id") Long id, @RequestBody Command command);

    @PostMapping("/command")
    Command command(@RequestHeader(AUTH_TOKEN) String token, @RequestBody Command command);

}
