package com.hexagone.frontend.controllers;

import com.hexagone.frontend.entities.Command;
import com.hexagone.frontend.entities.CommandLine;
import com.hexagone.frontend.entities.Product;
import com.hexagone.frontend.interfaces.CommandsFeignClient;
import com.hexagone.frontend.interfaces.ProductsFeignClient;
import org.bouncycastle.math.raw.Mod;
import org.keycloak.KeycloakSecurityContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.security.Principal;
import java.util.Optional;

@Controller
public class WebController {

    /** Permet d'utiliser les fonctions de l'interface ProductsFeignClient */
    @Autowired
    ProductsFeignClient productsFeignClient;

    /** Permet d'utiliser les fonctions de l'interface commandFeignClient */
    @Autowired
    CommandsFeignClient commandFeignClient;

    /** Permet l'affichage de la page d'accueil de l'application
     * @return "home" */
    @GetMapping("/")
    public String getHome() {
        return "home";
    }

    /**
     * Cette fonction a pour but d'afficher la liste des produits
     * @param model est le model du produit
     * @param request permet la connexion à Keycloak
     * @return "productlist" */
    @GetMapping("/productlist")
    public String getProductList(HttpServletRequest request, Model model){
        KeycloakSecurityContext context = (KeycloakSecurityContext) request.getAttribute(KeycloakSecurityContext.class.getName());
        String token = context.getTokenString();
        Iterable<Product> productList = productsFeignClient.products("Bearer "+token);
        model.addAttribute("productList", productList);
        return "productlist";
    }
//
//    /** Cette fonction s'utilise sur la page "productlist" pour permettre d'ajouter des produits au panier en créant des lignes de commande
//     * @param model est le model de la ligne de commande
//     * @param commandLine est la ligne de commande contenant l'ID et la quantité du produit
//     * @return "prodcutlist" */
//    @PostMapping("/productlist")
//    public String postCommandLine(Model model, CommandLine commandLine){
//        CommandLine commandLine1 = commandFeignClient.commandLine(commandLine);
//        model.addAttribute("commandLine", commandLine1);
//        return "redirect:/productlist";
//    }

    /** Celle-ci permet d'afficher les informations spécifique au produit grâce à son ID
     * @param id est l'ID du produit
     * @param model est le model du produit
     * @param request permet la connexion à Keycloak
     * @return "product" */
    @GetMapping ("/product/{id}")
    public String getProduct(HttpServletRequest request,@PathVariable("id") Long id, Model model){
        KeycloakSecurityContext context = (KeycloakSecurityContext) request.getAttribute(KeycloakSecurityContext.class.getName());
        String token = context.getTokenString();
        Optional<Product> product = productsFeignClient.productId("Bearer "+token, id);
        model.addAttribute("product", product.get());
        return "product";
    }

    /** Cette fonction permet de supprimer un produit à l'aide de son ID
     * @param id est l'ID du produit
     * @param request permet la connexion à Keycloak
     * @return "redirect:/productlist" */
    @GetMapping("/product/delete/{id}")
    public String deleteProduct(HttpServletRequest request, @PathVariable("id") Long id) {
        KeycloakSecurityContext context = (KeycloakSecurityContext) request.getAttribute(KeycloakSecurityContext.class.getName());
        String token = context.getTokenString();
        Product product = productsFeignClient.deleteProduct("Bearer "+token, id);
        return "redirect:/productlist";
    }

    /** La fonction permet de faire une redirection vers la page "update" pour permettre la mise à jour du produit
     * @param id est l'ID du produit
     * @param request permet la connexion à Keycloak
     * @param model est le model du produit
     * @return "update" */
    @GetMapping("/update/{id}")
    public String getUpdate(HttpServletRequest request, @PathVariable("id") Long id, Model model){
        KeycloakSecurityContext context = (KeycloakSecurityContext) request.getAttribute(KeycloakSecurityContext.class.getName());
        String token = context.getTokenString();
        Optional<Product> product = productsFeignClient.productId("Bearer "+token, id);
        model.addAttribute("product", product.get());
        return "update";
    }

    /** La fonction permet d'effectuer la mise à jour d'un produit grâce à son ID et d'effectuer une redirection vers la page "home"
     * @param id est l'ID du produit
     * @param request permet la connexion à Keycloak
     * @param model est le model su produit
     * @param product est le produit
     * @return "redirect:/product/{id}"
     */
    @PostMapping("/update/{id}")
    public String putProduct(HttpServletRequest request, @PathVariable("id") Long id, Model model, Product product){
        KeycloakSecurityContext context = (KeycloakSecurityContext) request.getAttribute(KeycloakSecurityContext.class.getName());
        String token = context.getTokenString();
        Product productOpt = productsFeignClient.putProduct("Bearer "+token, id, product);
        model.addAttribute("product", productOpt);
        return "redirect:/product/{id}";
    }

    /** La fonction permet de faire une redirection vers la page "addProduct" pour permettre la création d'un nouveau produit
     * @param model est le model du produit
     * @return "addProduct"
     */
    @GetMapping("/addProduct")
    public String getAddProduct(Model model){
        model.addAttribute("product", new Product());
        return "addProduct";
    }

    /** La fonction permet de créer un nouveau produit et une fois cela fait rediriger l'utilisateur vers la page "home"
     * @param model est le model
     * @param product est le produit
     * @param request permet la connexion à Keycloak
     * @return "home"
     */
    @PostMapping("/addProduct")
    public String postAddProduct(HttpServletRequest request, Model model, @ModelAttribute Product product){
        KeycloakSecurityContext context = (KeycloakSecurityContext) request.getAttribute(KeycloakSecurityContext.class.getName());
        String token = context.getTokenString();
        Product prod = productsFeignClient.product("Bearer "+token, product);
        model.addAttribute("product", prod);
        return "redirect:/";
    }

    /**
     * Permet l'affichage du panier pour une commande
     * @param principal pour afficher le nom de l'user
     * @param model est le model de la ligne de commande
     * @param request permet la connexion à Keycloak
     * @return "basket"
     */
    @GetMapping("/basket")
    public String getCommandLines(HttpServletRequest request, Command command, Principal principal, Model model){
        KeycloakSecurityContext context = (KeycloakSecurityContext) request.getAttribute(KeycloakSecurityContext.class.getName());
        String token = context.getTokenString();
        Iterable<CommandLine> commandLines = commandFeignClient.commandlines("Bearer "+token);
        Iterable<Product> productList = productsFeignClient.products("Bearer "+token);
        model.addAttribute("username", principal.getName());
        model.addAttribute("basket", commandLines);
        model.addAttribute("command", command);
        model.addAttribute("productList", productList);
        return "basket";
    }

    /**
     * Permet d'afficher toute les commandes
     * @param request permet la connexion à Keycloak
     * @return "commands"
     */
    @GetMapping("/commands")
    public String getCommands(HttpServletRequest request, Model model){
        KeycloakSecurityContext context = (KeycloakSecurityContext) request.getAttribute(KeycloakSecurityContext.class.getName());
        String token = context.getTokenString();
        Iterable<Command> commands = commandFeignClient.commands("Bearer "+token);
        model.addAttribute("commands", commands);
        return "commands";
    }

    /** Celle-ci permet d'afficher les informations spécifique à la commande grâce à son ID
     * @param id est l'ID de la commande
     * @param model est le model de la commande
     * @param request permet la connexion à Keycloak
     * @return "command" */
    @GetMapping ("/command/{id}")
    public String getCommand(HttpServletRequest request, @PathVariable("id") Long id, Model model){
        KeycloakSecurityContext context = (KeycloakSecurityContext) request.getAttribute(KeycloakSecurityContext.class.getName());
        String token = context.getTokenString();
        Optional<Command> command = commandFeignClient.commandId("Bearer "+token, id);
        model.addAttribute("command", command.get());
        return "command";
    }

    /** Cette fonction permet de supprimer une commande à l'aide de son ID
     * @param request permet la connexion à Keycloak
     * @param id est l'ID de la command
     * @return "home" */
    @GetMapping("/command/delete/{id}")
    public String deleteCommand(HttpServletRequest request, @PathVariable("id") Long id) {
        KeycloakSecurityContext context = (KeycloakSecurityContext) request.getAttribute(KeycloakSecurityContext.class.getName());
        String token = context.getTokenString();
        Command command = commandFeignClient.deleteCommand("Bearer"+token, id);
        return "redirect:/commands";
    }

    /**
     * Sert à supprimer une ligne de commande avant de valider la commande
     * @param request permet la connexion à Keycloak
     * @param id est l'ID de la ligne de commande
     * @return
     */
    @GetMapping("/commandline/delete/{id}")
    public String deleteCommandLine(HttpServletRequest request, @PathVariable("id") Long id) {
        KeycloakSecurityContext context = (KeycloakSecurityContext) request.getAttribute(KeycloakSecurityContext.class.getName());
        String token = context.getTokenString();
        CommandLine commandLine = commandFeignClient.deleteCommandLine("Bearer "+token, id);
        return "redirect:/basket";
    }

    /**
     * Permet d'afficher la page commandUpate
     * @param request permet la connexion à Keycloak
     * @param id est l'ID de la commande
     * @param model est le model de la commande
     * @return "commandUpdate"
     */
    @GetMapping("/commandUpdate/{id}")
    public String getCommandUpdate(HttpServletRequest request, @PathVariable("id") Long id, Model model){
        KeycloakSecurityContext context = (KeycloakSecurityContext) request.getAttribute(KeycloakSecurityContext.class.getName());
        String token = context.getTokenString();
        Optional<Command> command = commandFeignClient.commandId("Bearer "+token, id);
        model.addAttribute("command", command.get());
        return "commandUpdate";
    }

    /**
     * Sert à mettre à jour une commande mais le html thymleaf n'est pas fini
     * @param request permet la connexion à Keycloak
     * @param id est l'ID de la commande
     * @param model est le model de la commande
     * @return "redirect:/command/{id}"
     */
    @PostMapping("/commandUpdate/{id}")
    public String putCommand(HttpServletRequest request, @PathVariable("id") Long id, Model model, Command command){
        KeycloakSecurityContext context = (KeycloakSecurityContext) request.getAttribute(KeycloakSecurityContext.class.getName());
        String token = context.getTokenString();
        Command commandOpt = commandFeignClient.putCommand("Bearer "+token, id, command);
        model.addAttribute("command", commandOpt);
        return "redirect:/command/{id}";
    }


    /**
     * Sert à créer une commande
     * @param request permet la connexion à Keycloak
     * @param model est le model de la commande
     * @param command est l'Entitée de la commande
     * @param bindingResult récupere le résultat de mes methodes
     * @return
     */
    @PostMapping("/addCommand")
    public String postAddCommand(HttpServletRequest request, Model model, Command command, BindingResult bindingResult){
        KeycloakSecurityContext context = (KeycloakSecurityContext) request.getAttribute(KeycloakSecurityContext.class.getName());
        String token = context.getTokenString();
        Command command1 = commandFeignClient.command("Bearer "+token, command);
        model.addAttribute("product", command1);
        return "redirect:/";
    }

    /**
     *
     * @param request permet la connexion à Keycloak
     * @param principal pour avoir le nom de l'utilisateur
     * @param model est le model de la commande
     * @param id est l'ID de la commande
     * @return "addCommandLine"
     */
    @GetMapping("/addCommandLine/{id}")
    public String getAddCommandLine(HttpServletRequest request, Principal principal, Model model, @PathVariable("id") Long id){
        KeycloakSecurityContext context = (KeycloakSecurityContext) request.getAttribute(KeycloakSecurityContext.class.getName());
        String token = context.getTokenString();
        model.addAttribute("username", principal.getName());
        model.addAttribute("commandLine", new CommandLine());
        Optional<Product> product = productsFeignClient.productId("Bearer "+token, id);
        model.addAttribute("product", product.get());
        return "addCommandLine";
    }

    /**
     *
     * @param request permet la connexion à Keycloak
     * @param model est le model de la ligne de commande
     * @param commandLine est l'entitée de ligne de commande
     * @return
     */
    @PostMapping("/addCommandLine")
    public String postCommandLine(HttpServletRequest request, Model model, CommandLine commandLine){
        KeycloakSecurityContext context = (KeycloakSecurityContext) request.getAttribute(KeycloakSecurityContext.class.getName());
        String token = context.getTokenString();
        CommandLine commandLine1 = commandFeignClient.commandLine("Bearer "+token, commandLine);
        model.addAttribute("commandLine", commandLine1);
        return "redirect:/productlist";
    }

}