package com.hexagone.frontend.entities;

public class CommandLine{

    private Long id;

    private Long productId;

    private Integer quantity;

    private String customer;

    private Boolean ordered;

    /**
     * Permet de recupèrer le nom du client
     * @return customer
     */
    public String getCustomer() {
        return customer;
    }

    /**
     * Permet de sauvegarder le nom du client
     * @param customer nom du client
     */
    public void setCustomer(String customer) {
        this.customer = customer;
    }

    /**
     * Permet de recupèrer l'état
     * @return ordered
     */
    public Boolean getOrdered() {
        return ordered;
    }

    /**
     * permet de sauvegarder l'état de la ligne commande pour savoir si cette ligne de commande est dans une commande ou non
     * @param ordered état de la ligne de commande
     */
    public void setOrdered(Boolean ordered) {
        this.ordered = ordered;
    }

    /**
     * Permet de recupèrer l'id
     * @return id
     */
    public Long getId() {
        return id;
    }

    /**
     * Permet de sauvegarder l'id
     * @param id ID de la ligne de commande
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * Permet de recupèrer l'id du produit
     * @return productId
     */
    public Long getProductId() {
        return productId;
    }

    /**
     * Permet de sauvegarder l'id du produit
     * @param productId id du produit lié
     */
    public void setProductId(Long productId) {
        this.productId = productId;
    }

    /**
     * Permet de recupèrer la quantité
     * @return quantity
     */
    public Integer getQuantity() {
        return quantity;
    }

    /**
     * Permet de sauvegarder  la quantité du du produit
     * @param quantity quantité du produit
     */
    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }

    /**
     * Permet de mettre les éléments en string
     * @return CommandLine
     */
    @Override
    public String toString() {
        return "CommandLine{" +
                "id=" + id +
                ", productId=" + productId +
                ", quantity=" + quantity +
                ", customer='" + customer + '\'' +
                ", ordered=" + ordered +
                '}';
    }
}