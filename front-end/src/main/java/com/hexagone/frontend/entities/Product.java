package com.hexagone.frontend.entities;

public class Product extends CommandLine {

    /**
     * L'ID du produit
     */
    private Long id;

    /**
     * Le nom du produit
     */
    private String name;

    /** La durée de validité du produit
     */
    private String validateTime;

    /** Les instructions d’utilisation du produit
     */
    private String instruction;

    /**
     * Permet de recupèrer l'id
     * @return id
     */
    public Long getId() {
        return id;
    }

    /**
     * Permet de sauvegarder l'id
     * @param id est l'ID du produit
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * Permet de recupèrer le nom
     * @return name
     */
    public String getName() {
        return name;
    }

    /**
     * Permet de sauvegarder le nom
     * @param name est le nom du produit
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * Permet de recupèrer la date de validité
     * @return validateTime
     */
    public String getValidateTime() {
        return validateTime;
    }

    /**
     *Permet de sauvegarder la date de validité
     * @param validateTime est la date de validité du produit
     */
    public void setValidateTime(String validateTime) {
        this.validateTime = validateTime;
    }

    /**
     * Permet de recupèrer les instruction
     * @return instruction
     */
    public String getInstruction() {
        return instruction;
    }

    /**
     * Permet de sauvegarder les instruction
     * @param instruction sont les instructions d'utilisation du produit
     */
    public void setInstruction(String instruction) {
        this.instruction = instruction;
    }
}
