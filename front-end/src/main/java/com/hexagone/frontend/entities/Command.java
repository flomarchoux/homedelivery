package com.hexagone.frontend.entities;

import java.util.List;

public class Command {

    private Long id;

    private List<CommandLine> commandLine;

    private String commandDate;

    private String address;

    private String  deliveryman;

    private String status;

    private String customer;

    /**
     * Permet de recupèrer l'id
     * @return id
     */
    public Long getId() {
        return id;
    }

    /**
     * permet de sauvegarder l'id
     * @param id est l'id de la commande
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * Permet de recupèrer la ligne de commande
     * @return commandline
     */
    public List<CommandLine> getCommandLine() {
        return commandLine;
    }

    /**
     * permet de sauvegarder la ligne de commande
     * @param commandLine est la ligne de commande
     */
    public void setCommandLine(List<CommandLine> commandLine) {
        this.commandLine = commandLine;
    }

    /**
     * Permet de recupèrer la date de commande
     * @return commandDate
     */
    public String getCommandDate() {
        return commandDate;
    }

    /**
     * permet de sauvegarder la date de la commande
     * @param commandDate est la date de la commande
     */
    public void setCommandDate(String commandDate) {
        this.commandDate = commandDate;
    }

    /**
     * Permet de recupèrer l'id
     * @return address
     */
    public String getAddress() {
        return address;
    }

    /**
     * permet de sauvegarder l'adresse
     * @param address est l'adresse de livraison
     */
    public void setAddress(String address) {
        this.address = address;
    }

    /**
     * Permet de recupèrer le nom du livreur
     * @return deliveryman
     */
    public String getDeliveryman() {
        return deliveryman;
    }

    /**
     * permet de sauvegarder le nom du livreur
     * @param deliveryman est le nom du livreur
     */
    public void setDeliveryman(String deliveryman) {
        this.deliveryman = deliveryman;
    }

    /**
     * Permet de recupèrer le status de la commande
     * @return status
     */
    public String getStatus() {
        return status;
    }

    /**
     * permet de sauvegarder l'état de la commande
     * @param status est le statut de la comande
     */
    public void setStatus(String status) {
        this.status = status;
    }

    /**
     * Permet de recupèrer le nom du client
     * @return customer
     */
    public String getCustomer() {
        return customer;
    }

    /**
     * permet de sauvegarder le nom du client
     * @param customer est le nom du client
     */
    public void setCustomer(String customer) {
        this.customer = customer;
    }
}