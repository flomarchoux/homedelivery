# Home Delivery Project (Sur MAC M1)

#### *Installation de Keycloak pour l'authentification :*

```shell
docker pull wizzn/keycloak:12
docker run -p 8081:8080 --name keycloak wizzn/keycloak:12
```

#### *Paramétrage de Keycloak :*

```shell
docker exec keycloak /opt/jboss/keycloak/bin/add-user-keycloak.sh -u admin -p admin
docker restart keycloak
```
