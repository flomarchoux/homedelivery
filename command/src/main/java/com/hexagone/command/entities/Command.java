package com.hexagone.command.entities;

import javax.persistence.*;
import java.util.Date;
import java.util.List;

@Entity
public class Command {

    @Id
    @GeneratedValue
    private Long id;

    @OneToMany(cascade = CascadeType.REMOVE)
    private List<CommandLine> commandLine;

    private String commandDate;

    private String address;

    private String  deliveryman;

    private String status;

    private String customer;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public List<CommandLine> getCommandLine() {
        return commandLine;
    }

    public void setCommandLine(List<CommandLine> commandLine) {
        this.commandLine = commandLine;
    }

    public String getCommandDate() {
        return commandDate;
    }

    public void setCommandDate(String commandDate) {
        this.commandDate = commandDate;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getDeliveryman() {
        return deliveryman;
    }

    public void setDeliveryman(String deliveryman) {
        this.deliveryman = deliveryman;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getCustomer() {
        return customer;
    }

    public void setCustomer(String customer) {
        this.customer = customer;
    }
}