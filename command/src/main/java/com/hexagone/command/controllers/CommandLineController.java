package com.hexagone.command.controllers;

import com.hexagone.command.entities.CommandLine;
import com.hexagone.command.repository.CommandLineRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.persistence.EntityNotFoundException;
import java.util.Optional;

@RestController
public class CommandLineController {

    /**
     * Permet d'utiliser les fonctions de l'interface CommandLineRepository
     */
    @Autowired
    private CommandLineRepository commandLineRepository;

    /**
     * Permet d'afficher les lignes de commande
     * @return commandLines
     */
    @GetMapping("/commandlines")
    public Iterable<CommandLine> getCommandLines(){
        Iterable<CommandLine> commandLines = commandLineRepository.findAll();
        return commandLines;
    }

    /**
     * permet d'afficher une ligne de commande par rapport à son id
     * @param id ID de la commandline
     * @return
     */
    @GetMapping(path = "/commandline/{id}")
    public Optional<CommandLine> getCommandLine(@PathVariable("id") Long id){
        Optional<CommandLine> commandLine = commandLineRepository.findById(id);
        return commandLine;
    }

    /**
     * Permet de créer une ligne de commande
     * @param commandLine L'entité commandline
     * @return
     */
    @PostMapping("/commandline")
    public CommandLine postCommandLine(
            @RequestBody
            CommandLine commandLine
    ){
        commandLine = commandLineRepository.save(commandLine);
        return commandLine;
    }

    /**
     * Permet de mettre à jour une ligne de commande en fonction de son id
     * @param id ID de la ligne de commande
     * @param commandLine L'entité commandline
     * @return command
     */
    @PutMapping("/commandline/update/{id}")
    public CommandLine putCommandLine(
            @PathVariable("id")
                    Long id,
            @RequestBody
                    CommandLine commandLine
    ){
        Optional<CommandLine> CommandLineOpt = commandLineRepository.findById(id);
        return CommandLineOpt.map(currentProduct -> {
            commandLine.setId(currentProduct.getId());
            return commandLineRepository.save(commandLine);
        }).orElseThrow(EntityNotFoundException::new);
    }

    /**
     * Permet de supprimer une ligne de commande en fonction de son id
     * @param id ID de la ligne de commande
     * @return null
     */
    @DeleteMapping("/commandline/delete/{id}")
    public String deleteCommandLine(
            @PathVariable
                    Long id
    ){
        commandLineRepository.deleteById(id);
        return null;
    }

}