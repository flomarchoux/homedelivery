package com.hexagone.command.controllers;

import com.hexagone.command.entities.Command;
import com.hexagone.command.repository.CommandRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.persistence.EntityNotFoundException;
import java.util.Optional;

@RestController
public class CommandController {

    /**
     * Permet d'utiliser les fonctions de l'interface CommandRepository
     */
    @Autowired
    private CommandRepository commandRepository;

    /**
     * Permet d'afficher les commandes
     * @return commands
     */
    @GetMapping("/commands")
    public Iterable<Command> getCommands(){
        Iterable<Command> commands = commandRepository.findAll();
        return commands;
    }

    /**
     * permet d'afficher une commande par rapport à son id
     * @param id ID de la commande
     * @return
     */
    @GetMapping(path = "/command/{id}")
    public Optional<Command> getCommand(@PathVariable("id") Long id){
        Optional<Command> command = commandRepository.findById(id);
        return command;
    }

    /**
     * Permet de créer une commande
     * @param command L'entité command
     * @return
     */
    @PostMapping("/command")
    public Command postCommand(
            @RequestBody
            Command command
    ){
        command = commandRepository.save(command);
        return command;
    }

    /**
     * Permet de mettre à jour une commande en fonction de son id
     * @param id ID de la commande
     * @param command L'entité command
     * @return command
     */
    @PutMapping("/command/update/{id}")
        public Command putCommand(
                @PathVariable("id")
                        Long id,
                @RequestBody
                        Command command
    ){
        Optional<Command> CommandOpt = commandRepository.findById(id);
        return CommandOpt.map(currentProduct -> {
            command.setId(currentProduct.getId());
            return commandRepository.save(command);
        }).orElseThrow(EntityNotFoundException::new);
    }

    /**
     * Permet de supprimer une commande en fonction de son id
     * @param id ID de la commande
     * @return null
     */
    @DeleteMapping("/command/delete/{id}")
        public String deleteCommand(
            @PathVariable
                    Long id
    ){
        commandRepository.deleteById(id);
        return null;
    }
 }
