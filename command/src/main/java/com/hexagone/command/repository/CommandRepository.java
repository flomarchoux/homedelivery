package com.hexagone.command.repository;

import com.hexagone.command.entities.Command;
import org.springframework.data.repository.CrudRepository;

public interface CommandRepository extends CrudRepository<Command,Long> {
}
