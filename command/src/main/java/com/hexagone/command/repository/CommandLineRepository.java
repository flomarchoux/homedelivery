package com.hexagone.command.repository;

import com.hexagone.command.entities.CommandLine;
import org.springframework.data.repository.CrudRepository;

public interface CommandLineRepository extends CrudRepository<CommandLine,Long> {
}
